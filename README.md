# mod2midi2mdx-mml: repository for collecting tools for converting from mod to midi to MXDRV (MDX) variant of MML

2midi: convert mod to midi

MIDI2MML.exe: convert from midi to almost MXDRV compatible MML (easy for fix by hand)

Note that before using MIDI2MML.exe you should probably use some midi composing software to normalize the note placing and lenght.
Cakewalk is a free option.
